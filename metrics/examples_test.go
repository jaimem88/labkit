package metrics_test

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/gitlab-org/labkit/metrics"
)

func ExampleNewHandlerFactory() {
	// Tell prometheus to include a "route" label for the http metrics
	newMetricHandlerFunc := metrics.NewHandlerFactory(metrics.WithLabels("route"))

	http.Handle("/foo",
		newMetricHandlerFunc(
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintf(w, "Hello foo")
			}),
			metrics.WithLabelValues(map[string]string{"route": "/foo"}), // Add instrumentation with a custom label of route="/foo"
		))

	http.Handle("/bar",
		newMetricHandlerFunc(
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintf(w, "Hello bar")
			}),
			metrics.WithLabelValues(map[string]string{"route": "/bar"}), // Add instrumentation with a custom label of route="/bar"
		))

	log.Fatal(http.ListenAndServe(":8080", nil))
}
